# Disable Wiki Page Previews

Disables Wikipedia's page previews that pop up when hovering over a link with the mouse.
